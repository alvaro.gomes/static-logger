package br.com.ajbg.staticlogger;

import java.io.PrintStream;
import java.io.FileOutputStream;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.Date;
import java.util.MissingFormatArgumentException;

public class Log {
	public static final int NONE = -1;
	public static final int ERROR = 0;
	public static final int WARNING = 1;
	public static final int INFO = 2;
	public static final int DEBUG = 3;

	private static BlockingQueue<String> queue;
	private static LogWriter writer;

	private static int logLevel;
	private static String logFile;
	private static String mainPat;

	static {
		queue = new LinkedBlockingDeque<String>();
		writer = new LogWriter(queue);
		writer.start();

		logLevel = Integer.getInteger(
				"br.com.ajbg.staticlogger.level", 2);
		logFile = "logfile.log";
		mainPat = "[%1$tY-%1$tm-%1$td %1$tH:%1$tM:%1$tS] (%2$s) ";
	}

	public static void level(int logLevel) {
		Log.logLevel = logLevel;
	}

	public static void file(String logFile) {
		Log.logFile = logFile;
	}

	public static void format(String mainPat) {
		Log.mainPat = mainPat;
	}

	private static void log(String message, String type) {
		String main = String.format(mainPat, new Date(), type);
		queue.add(main + message + "\n");
		if (writer.getState() == Thread.State.TERMINATED) {
			writer = new LogWriter(queue);
			writer.start();
		}
	}

	public static void debug(String pattern, String type, Object... msg) {
		if (logLevel >= DEBUG) {
			try {
				log(String.format(pattern, msg), type);
			} catch (MissingFormatArgumentException e) {
				log(String.format(pattern, type), "DEBUG");
			}
		}
	}

	public static void debug(String pattern, Object... msg) {
		debug(pattern, "DEBUG", msg);
	}

	public static void info(String pattern, String type, Object... msg) {
		if (logLevel >= INFO) {
			try {
				log(String.format(pattern, msg), type);
			} catch (MissingFormatArgumentException e) {
				log(String.format(pattern, type), "INFO");
			}
		}
	}

	public static void info(String pattern, Object... msg) {
		info(pattern, "INFO", msg);
	}

	public static void warning(String pattern, String type, Object... msg) {
		if (logLevel >= WARNING) {
			try {
				log(String.format(pattern, msg), type);
			} catch (MissingFormatArgumentException e) {
				log(String.format(pattern, type), "WARN");
			}
		}
	}

	public static void warning(String pattern, Object... msg) {
		warning(pattern, "WARN", msg);
	}

	public static void error(String pattern, String type, Object... msg) {
		if (logLevel >= ERROR) {
			try {
				log(String.format(pattern, msg), type);
			} catch (MissingFormatArgumentException e) {
				log(String.format(pattern, type), "ERROR");
			}
		}
	}

	public static void error(String pattern, Object... msg) {
		error(pattern, "ERROR", msg);
	}

	static class LogWriter extends Thread {
		private BlockingQueue<String> queue;

		public LogWriter(BlockingQueue<String> queue) {
			this.queue = queue;
		}

		public void run() {
			try {
				PrintStream ps = new PrintStream(
					new FileOutputStream(logFile, true));
				while (queue.peek() != null) {
					ps.print(queue.take());
				}
				ps.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
